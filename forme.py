import cv2 as cv
import numpy as np
import os
import help

true_symbol = 1
false_symbol = 0
not_filled = r"N\A"
doubt = 'd'
extensions = [".jpeg", ".jpg", ".png"]

""" This is the very important function which scans the page and aligns it to the template. 
Also it's in this function where we return the raw array containing the number of pixels for each form and 
question which is later treated separetely by other auxillary functions"""
def get_answers(path, template, destList, coordinates):
    h, w, c = template.shape
    orb = cv.ORB_create(1000)
    kp1, des1 = orb.detectAndCompute(template, None)

    myPicList = os.listdir(path)

    for j, y in enumerate(myPicList):
        q2PixThresh = []
        q2CheckPixs = []
        q3PixThresh = []
        q3CheckPixs = []
        q4PixThresh = []
        q4CheckPixs = []
        q5PixThresh = []
        q5CheckPixs = []
        q6PixelThresh = []
        q6CheckPixs = []
        q7PixelThresh = []
        q7CheckPixs = []
        form_answers = []

        img = cv.imread(path + "/" + y)

        kp2, des2 = orb.detectAndCompute(img, None)
        bf = cv.BFMatcher(cv.NORM_HAMMING)
        matches = bf.match(des2, des1)
        matches.sort(key=lambda x: x.distance)
        good = matches[:int(len(matches) * (25 / 100))]

        scrPoints = np.float32([kp2[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dstPoints = np.float32([kp1[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, _ = cv.findHomography(scrPoints, dstPoints, cv.RANSAC, 5.0)
        imgScan = cv.warpPerspective(img, M, (w, h))

        imgShow = imgScan.copy()
        imgMask = np.zeros_like(imgShow)

        print(f"********** Extracting data from {myPicList[j]} **********")

        for x, r in enumerate(coordinates):
            cv.rectangle(imgMask, (r[0][0], r[0][1]), (r[1][0], r[1][1]), (0, 255, 0), cv.FILLED)
            imgShow = cv.addWeighted(imgShow, 0.99, imgMask, 0.1, 0)

            imgCrop = imgScan[r[0][1]:r[1][1], r[0][0]:r[1][0]]

            kernel = np.ones((4, 4), np.uint8)
            imgGray = cv.cvtColor(imgCrop, cv.COLOR_BGR2GRAY)
            blurImg = cv.blur(imgGray, (2, 2))
            _, otsu = cv.threshold(blurImg, 0, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)
            opening = cv.morphologyEx(otsu, cv.MORPH_OPEN, kernel)

            totalPixels = cv.countNonZero(opening)
            checkPixels = cv.countNonZero(otsu)

            if r[2] == '3':
                q3PixThresh.append(totalPixels)
                q3CheckPixs.append(checkPixels)
            elif r[2] == '4':
                q4PixThresh.append(totalPixels)
                q4CheckPixs.append(checkPixels)
            elif r[2] == '6':
                q6PixelThresh.append(totalPixels)
                q6CheckPixs.append(checkPixels)
            elif r[2] == '7':
                q7PixelThresh.append(totalPixels)
                q7CheckPixs.append(checkPixels)
            elif r[2] == '1':
                if totalPixels < 5 and checkPixels > 145:
                    totalPixels = 20
                help.fill_first_question(totalPixels, form_answers)
            elif r[2] == '2':
                q2PixThresh.append(totalPixels)
                q2CheckPixs.append(checkPixels)
            elif r[2] == '5':
                q5PixThresh.append(totalPixels)
                q5CheckPixs.append(checkPixels)

        if len(q2PixThresh) > 0:
            help.check_boxes1(q2CheckPixs, q2PixThresh, form_answers,2)
        
        if len(q3PixThresh) > 0:
            help.check_boxes1(q3CheckPixs, q3PixThresh,form_answers,1)
        if len(q4PixThresh) > 0:
            help.check_boxes1(q4CheckPixs,q4PixThresh,form_answers,1)
        
        if len(q5PixThresh) > 0:
            help.check_boxes1(q5CheckPixs,q5PixThresh,form_answers,5)
       
        if len(q6PixelThresh) > 0:
            help.check_boxes1(q6CheckPixs,q6PixelThresh, form_answers,1)

        if len(q7PixelThresh) > 0:
            help.check_boxes1(q7CheckPixs, q7PixelThresh,form_answers,7)

        form_answers.insert(0, myPicList[j])
        destList.append(form_answers)








