import cv2 as cv
import help
import os
import forme
from tkinter import *
from tkinter import filedialog


true_symbol = 1
false_symbol = 0
not_filled = r'N\A'
doubt = 'd'
extensions = [".jpeg", ".jpg", ".png"]

root = Tk()
root.withdraw()

folder_images= filedialog.askdirectory(title="Select image folder")
folder_template = filedialog.askdirectory(title="Select template folder")

dest = filedialog.askdirectory(title="Select (or create) folder where to place doubts")
dest = help.get_path(dest)

scans_path = help.get_path(folder_images)
scan_list = os.listdir(scans_path)

coordinates = [help.coordinates_1, help.coordinates_2]

template_path = help.get_path(folder_template)
template_pics = os.listdir(template_path)

final_answers = []
result = []


for i in range(len(template_pics)):
    scan = scans_path + '/' + scan_list[i]
    template = cv.imread(template_path + '/' + template_pics[i])
    forme.get_answers(scan, template, final_answers, coordinates[i])

help.merge_lists(final_answers,result,0)
to_correct = help.get_question_number(result,doubt)
help.create_folder(dest)
help.to_correct_image(to_correct,scans_path,doubt,dest)

filename = input("Type the csv file name (without the csv postfix): ")
help.write_to_file(filename +'.csv',result,help.form_options)
input("Press any key to close")

