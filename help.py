import cv2 as cv
import numpy as np
import os
import shutil
from PIL import Image
from pdf2image import convert_from_path

true_symbol = 1
false_symbol = 0
not_filled = r"N\A"
doubt = 'd'
extensions = [".jpeg", ".jpg", ".png"]


form_options = ['',
                'Consent',
                'Cost',
                'Treatment',
                'Consumption',
                'Dimensions',
                'Risks',
                'Question 3',
                'Question 4',
                'Diabetes',
                'Alcoholism',
                'Breast Cancer',
                'Intelligence',
                'Blood type',
                'Age',
                'Gender']


doubt_coo = [[(122, 792), (1464, 1007), '1', 'q1'],
             [(117, 1002), (1482, 1447), '2', 'q2'],
             [(117, 1437), (1494, 1844), '3', 'q3'],
             [(124, 1819), (1482, 2189), '4', 'q4'],
             [(102, 317), (1482, 737), '5', 'q5'],
             [(87, 724), (1517, 1279), '6', 'q6'],
             [(97, 1267), (1472, 1582), '7', 'q7']]



coordinates_1 = [[(595, 680), (645, 735), '1', 'Consensus'],
                 [(166, 850), (216, 874), '2', 'q2_1'],
                 [(169, 885), (216, 909), '2', 'q2_2'],
                 [(166, 921), (216, 945), '2', 'q2_3'],
                 [(166, 956), (216, 985), '2', 'q2_4'],
                 [(166, 1018), (216, 1058), '2', 'q2_5'],
                 [(166, 1200), (216, 1233), '3', 'q3_1'],
                 [(166, 1240), (216, 1268), '3', 'q3_2"'],
                 [(166, 1274), (216, 1303), '3', 'q3_3'],
                 [(166, 1310), (216, 1343), '3', 'q3_4'],
                 [(166, 1460), (216, 1490), '4', 'q4_1'],
                 [(166, 1497), (216, 1520), '4', 'q4_2'],
                 [(166, 1533), (216, 1555), '4', 'q4_3'],
                 [(166, 1565), (216, 1596), '4', 'q4_4']]

coordinates_2 = [[(545, 350), (583, 373), '5', 'q5_1v'],
                 [(546, 386), (583, 408), '5', 'q5_2v'],
                 [(546, 421), (583, 443), '5', 'q5_3v'],
                 [(548, 456), (583, 478), '5', 'q5_4v'],
                 [(546, 491), (588, 531), '5', 'q5_5v'],
                 [(693, 350), (731, 373), '5', 'q5_1f'],
                 [(693, 386), (733, 408), '5', 'q5_2f'],
                 [(695, 420), (733, 443), '5', 'q5_3f'],
                 [(698, 455), (731, 478), '5', 'q5_4f'],
                 [(698, 491), (731, 523), '5', 'q5_5f'],
                 [(165, 640), (210, 667), '6', 'q6_1'],
                 [(165, 675), (210, 703), '6', 'q6_2'],
                 [(165, 710), (210, 737), '6', 'q6_3'],
                 [(163, 744), (210, 773), '6', 'q6_4'],
                 [(163, 780), (210, 808), '6', 'q6_5'],
                 [(165, 814), (210, 841), '6', 'q6_6'],
                 [(165, 850), (210, 880), '6', 'q6_7'],
                 [(165, 886), (210, 920), '6', 'q6_8'],
                 [(172, 1043), (210, 1060), '7', 'q7_1'],
                 [(172, 1077), (210, 1095), '7', 'q7_2'],
                 [(172, 1110), (210, 1135), '7', 'q7_3']]


# path should end with /
'''This function is used to create a folder given the path. If folder exits then the files
are added '''
def create_folder(name):
    finalPath = name
    try:
        os.mkdir(finalPath)
    except OSError:
        print(name + ' folder could not be created! Maybe it exists. \nFiles will be added to the existing folder!')
        return False
    else:
        print(name + " folder created.")
        return True

''' This function takes a pdf and converts it into images. It takes as parameters the 
 location where to place images, the original pdf location, the exact page folder and the page name'''
def pdf_to_image(locationFolder, pdfLocation, outputFolder, pageName):
    locationFolder = check_slash(locationFolder)
    outputFolder = check_slash(outputFolder)
    create_folder(locationFolder + outputFolder)
    images = convert_from_path(pdfLocation)
    suffix = '.jpg'
    p1 = 0
    for p in range(len(images)):
        dst = str(int(pageName) + p1) + suffix
        dst = dst.zfill(len(suffix) + len(pageName))
        path2 = locationFolder + outputFolder + dst
        images[p].save(path2, 'JPEG')
        p1 += 1
    print("Last added file name: " + dst)

""" This function allows us to copy files form one directory to another """
def copy(src, dst):
    if os.path.isdir(dst):
        dst = os.path.join(dst, os.path.basename(src))
    shutil.copyfile(src, dst)

""" Once we have the final list which contains the answers of each form, using this function 
 we can get the number of question which has a doubt. It returns a list with the form name and 
 the question or questions where we have a doubt"""
def get_question_number(array, symbol):
    final = []
    value = 0
    for i in array:
        doubtss = []
        for j in range(len(i)):
            if i[j] == symbol:
                if j == 1:
                    doubtss.append(1)
                elif j > 1 and j <= 6:
                    doubtss.append(2)
                elif j == 7:
                    doubtss.append(3)
                elif j == 8:
                    doubtss.append(4)
                elif j>8 and j<=13:
                    doubtss.append(5)
                elif j == 14:
                    doubtss.append(6)
                elif j == 15:
                    doubtss.append(7)
        if len(doubtss) > 1:
            check = doubtss[0]
            for g in doubtss:
                if g != check:
                    value = doubt
                else:
                    value = g
            final.append([i[0],value])

        elif len(doubtss) == 1:
            value = doubtss[0]
            final.append([i[0],value])
    return final

""" Once we know which question we doubt, using the crop function and the coordinates we can
cut exactly that question out and save it as pdf"""
def crop_image(image, question_number):
    question_number = str(question_number)
    for x, r in enumerate(doubt_coo):
        if r[2] == question_number:
            image = image[r[0][1]:r[1][1], r[0][0]:r[1][0]]
    return image

""" Allows us to get the cwd"""
def get_path(fullPath):
    cwd = os.getcwd().replace("\\", "/")
    n = len(cwd) + 1
    newPath = fullPath[n:len(fullPath)]
    return newPath

""" Checks if a given input is a string"""
def is_str(mot):
    return type(mot) is str

"""This is the second most important function as it is in this function that we treat each question 
differently based on the input from the get_answers() function."""
def check_boxes1(checkPixs, totalPixs, dstList, oneOrMult):
    answers = []
    temp = checkPixs.copy()
    m = max(checkPixs)
    val = 0

    if m > 500:
        temp.remove(m)
        m = max(temp)
    comp = m // 1.2
    if m < 180:
        comp = m // 1.35
    if is_str(checkPixs[0]):
        checkPixs = checkPixs[1:]
    if is_str(totalPixs[0]):
        totalPixs = totalPixs[1:]

    for i in range(len(checkPixs)):
        if ((checkPixs[i] > comp and totalPixs[i] > 10) or totalPixs[i] > 15 or checkPixs[i] > 150) and checkPixs[
            i] < 500:
            answers.append(true_symbol)
        elif (0 <= totalPixs[i] <= 15 and checkPixs[i] < comp) or checkPixs[i] < comp:
            answers.append(false_symbol)
        else:
            answers.append(1000)
    if oneOrMult == 1:
        if np.sum(answers) == 1:
            for i in range(len(answers)):
                if answers[i] == 1:
                    dstList.append(i + 1)
        else:
            t = []
            tMax = max(totalPixs)
            tComp = tMax // 2
            tTotpixs = totalPixs.copy()
            tTotpixs.remove(tMax)
            if tMax == 0 or tMax < 15:
                dstList.append(not_filled)
                return
            if tMax > 400:
                dstList.append(doubt)
                return

            for k in range(len(tTotpixs)):
                if tTotpixs[k] > tComp:
                    val = true_symbol
                else:
                    val = false_symbol
                t.append(val)
            if np.sum(t) > 0:
                dstList.append(doubt)
            else:
                for i in range(len(totalPixs)):
                    if totalPixs[i] == tMax:
                        dstList.append(i + 1)

    if oneOrMult == 2:
        for i in answers:
            if i > 600:
                dstList.append(doubt)
            else:
                dstList.append(i)

    if oneOrMult == 5:
        t = []
        for i in range(len(answers)):
            if (answers[i] > 1 and totalPixs[i] < 10) or (answers[i] > 1 and checkPixs[i] < 145):
                t.append(false_symbol)
            else:
                t.append(answers[i])
        fill_q5(t, dstList)

    if oneOrMult == 7:
        max7T = max(totalPixs)
        max7C = max(checkPixs)
        comp7 = max7C // 1.3
        if max7T == 0:
            dstList.append(not_filled)
            return
        if max7T > 400:
            dstList.append(doubt)
            return
        t = checkPixs.copy()
        t.remove(max7C)
        for i in range(len(t)):
            if t[i] > comp7:
                dstList.append(doubt)
                return
        for j in range(len(checkPixs)):
            if checkPixs[j] == max7C:
                dstList.append(j + 1)
                return

"""Converts the images into pdf"""
def img_to_pdf(srcPath):
    srcPath = check_slash(srcPath)
    list = os.listdir(srcPath)
    for image1 in list:
        if image1[0] == 'p':
            temp = list.copy()
            temp.remove(image1)
            for image2 in temp:
                if image1[2:] == image2[2:]:
                    img1 = Image.open(srcPath + image1)
                    img2 = Image.open(srcPath + image2)

                    im1 = img1.convert('RGB')
                    im2 = img2.convert('RGB')

                    imgList = [im1]

                    im2.save((srcPath + str(image1[2:6]) + '.pdf'), save_all=True, append_images=imgList)

"""Takes the list of the questions to correct and then it crops those images and 
saves them into the where__to_put folder"""
def to_correct_image(list_of_questions, source_images,symbol_to_look,where_to_put):
    source = os.listdir(source_images)
    for i in list_of_questions:
        if i[1] != symbol_to_look:

            temp = int(i[1])
            if temp > 4:
                check_where = source_images + '/' + source[1]
            elif temp <= 4:
                check_where = source_images + '/' + source[0]
            images = os.listdir(check_where)
            for image in images:
                if image == i[0]:
                    image_temp = cv.imread(check_where + '/' + image)
                    cropped = crop_image(image_temp,i[1])
                    cv.imwrite(os.path.join(where_to_put, image), cropped)
        elif i[1] == symbol_to_look:
            num = 1
            for file in source:
                file = check_slash(file)
                pages = os.listdir(source_images + '/' + file)
                for page in pages:
                    if i[0] == page:
                        image_page = cv.imread(source_images + '/' + file + page)
                        cv.imwrite(os.path.join(where_to_put,'p' + str(num) + page),image_page)
                        num += 1
            img_to_pdf(where_to_put)
            delete_images(where_to_put,extensions)

""" Function created for the first question"""
def fill_first_question(pixels, destList):
    if pixels > 10:
        destList.append(true_symbol)
    elif pixels < 0:
        destList.append(doubt)
    else:
        destList.append(false_symbol)

"""This function is used to create the CSV file"""
def write_to_file(fileName, sourceArray, options):
    with open(fileName, 'a+') as f:
        for i in options:
            f.write(i + ',')
        f.write('\n')

        for data in sourceArray:
            for e in data:
                f.write((str(e) + ','))
            f.write('\n')

""" Allows us to fill the multiple answer questions, so it eliminates the possibility
to have more than one answer"""
def multiple(src, dest):
    for i in src:
        if i < 0 or i > 300:
            dest.append(doubt)
        elif i < 5:
            dest.append(false_symbol)
        else:
            dest.append(true_symbol)

""" If a question is completely empty then the result of it's array containing the number 
of pixels should be zero, this is the function that checks just that"""
def check_if_notFilled(src, dest):
    if np.sum(src) == 0:
        for i in range(len(dest)):
            dest[i] = not_filled

""" Function designed to fill question 5"""
def fill_q5(src, dest):
    vrai = src[:5].copy()
    faux = src[5:].copy()
    for i in range(len(vrai)):
        if vrai[i] > faux[i]:
            dest.append(true_symbol)
        elif faux[i] > vrai[i]:
            dest.append(false_symbol)
        elif vrai[i] == faux[i] == 0:
            dest.append(not_filled)
        else:
            dest.append(doubt)

""" We don't actually use this function, it is only to visalize which parts of the document
the program intends to scan"""
def find_boxes(path, template, coordinates):
    h, w, c = template.shape
    orb = cv.ORB_create(1000)
    kp1, des1 = orb.detectAndCompute(template, None)

    myPicList = os.listdir(path)

    for j, y in enumerate(myPicList):
        img = cv.imread(path + "/" + y)

        kp2, des2 = orb.detectAndCompute(img, None)
        bf = cv.BFMatcher(cv.NORM_HAMMING)
        matches = bf.match(des2, des1)
        matches.sort(key=lambda x: x.distance)
        good = matches[:int(len(matches) * (25 / 100))]

        scrPoints = np.float32([kp2[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dstPoints = np.float32([kp1[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, _ = cv.findHomography(scrPoints, dstPoints, cv.RANSAC, 5.0)
        imgScan = cv.warpPerspective(img, M, (w, h))

        imgShow = imgScan.copy()
        imgMask = np.zeros_like(imgShow)

        for x, r in enumerate(coordinates):
            cv.rectangle(imgMask, (r[0][0], r[0][1]), (r[1][0], r[1][1]), (0, 255, 0), cv.FILLED)
            imgShow = cv.addWeighted(imgShow, 0.99, imgMask, 0.1, 0)

        imgShow = cv.resize(imgShow, (w // 2, h // 2))
        cv.imshow(myPicList[j], imgShow)

""" This function takes the answers of the front page and the back page and combines them into 
one single array"""
def merge_lists(src, des, key_index):
    for i in range(len(src)):
        for j in range(i + 1, len(src)):
            if src[i][key_index] == src[j][key_index]:
                temp = src[j].copy()
                temp.remove(temp[0])
                des.append(src[i] + temp)

"""This is the function which renames all the images in a folder"""
def rename(directory, name, extension):
    os.chdir(directory)
    i = 1
    suffix = '.' + extension

    for file in os.listdir():
        src = file
        dst = str(int(name) + i) + suffix
        dst = dst.zfill(len(suffix) + len(name))
        os.rename(src, dst)
        i += 1

""" it only checks if a given word contains a slash or not, if not the slash is added"""
def check_slash(word):
    if word[-1] != '/':
        return word + '/'
    else:
        return word

""" If we have converted the images into pdf, then using this function we can delete thos images
which allows us to clean a little bit the results"""
def delete_images(src, extensions):
    list = os.listdir(src)
    for file in list:
        for e in extensions:
            if file.endswith(e) and file.startswith('p'):
                os.remove(os.path.join(src, file))




